#!/usr/bin/env python
import requests
import argparse
import MySQLdb as mysql
import json
import logging
import time

import xml.etree.ElementTree as ET

URL = "http://test.webjet.pro/api.php"
TRANSACTION = 100
TIMEOUT = 1

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

class APIException(Exception):
    def __init__(self, description):
        self.description = description
    def __str__(self):
        return repr(self.description)

def extract_info(filename):
    tree = ET.iterparse(filename)
    for event, elem in tree:
        if elem.tag == 'email':
            yield (int(elem.get('id')),
                   elem.find('to').text,
                   elem.find('subject').text)
            elem.clear()
        

def posting(eid, to, subject):
    params = {'id': eid,
              'message': json.dumps({'to': to, 'subject': subject})}
    r = requests.post(URL, data=params)
    return time.strftime('%Y-%m-%d %H:%M:%S'), r

def transactional_write(connector, table, objects):
    insert = ("INSERT IGNORE INTO %s VALUES"
              "(%s, %s, %s)") % (table, '%s', '%s', '%s')
    cur = connector.cursor()
    cur.executemany(insert, objects)
    connector.commit()

def read_configuration(config='config.secret'):
    with open(config, 'r') as json_config:
        c = json.load(json_config)
        return ({'unix_socket': c['unix-socket'],
                 'user': c['username'],
                 'passwd': c['password'],
                 'db': c['database']},
                 c['table'])

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="send to API "
                                     "data from XML file")
    parser.add_argument('file', help='XML file')
    args = parser.parse_args()
    connection_options, table = read_configuration()
    connector = mysql.connect(**connection_options)
    responses = []
    try:
        for i, (eid, to, subject) in enumerate(extract_info(args.file)):
            t, r = posting(eid, to, subject)
            
            while r.status_code == 503:
                logger.info('API busy: ' + str(eid))
                time.sleep(TIMEOUT)
                t, r = posting(eid, to, subject)

            if r.status_code == 200:
                logger.info('OK: ' + str(eid))
                responses.append((eid, t, int(r.text)))
            else:
                logger.error(r.text)
                raise APIException(r.text)

            if i % TRANSACTION == 0:
                transactional_write(connector, table, responses)
                responses = []
    finally:
        transactional_write(connector, table, responses)
